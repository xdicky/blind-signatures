from Crypto.Signature import PKCS1_PSS
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA

from PyBlindSchemes.Blindsignature.rsa_pss import PSS_BlindSigScheme
from cli.utils import generate_file_hash_obj

import os


def get_dir(my_dir='keys'):
    return os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, my_dir))


def sign_orig(message):
    sk_file = get_dir() + "/private_key.pem"
    key = RSA.importKey(open(sk_file).read())
    h = SHA256.new()
    h.update(message.encode())
    signer = PKCS1_PSS.new(key)
    signature = signer.sign(h)
    return signature


def verify_orig(message, signature, text):
    pk_file = get_dir() + "/public_key.pem"
    key = RSA.importKey(open(pk_file).read())
    h = SHA256.new()
    h.update(message.encode())
    verifier = PKCS1_PSS.new(key)
    if verifier.verify(h, signature):
        print(str(text) + " : The signature is authentic.")
    else:
        print(str(text) + " : The signature is not authentic.")


def original():
    message = 'hello world'
    signature = sign_orig(message)
    verify_orig(message, signature, "ORIGINAL SCHEME")


def blind(message):
    pk_file = get_dir() + "/public_key.pem"
    key = RSA.importKey(open(pk_file).read())
    blinder = PSS_BlindSigScheme(key)
    h = SHA256.new(message.encode())
    return blinder.blind_message(h)


def new_sign(em):
    sk_file = get_dir() + "/private_key.pem"
    key = RSA.importKey(open(sk_file).read())
    signer = PSS_BlindSigScheme(key)
    return signer.sign_message(em)


def unblind(blinded, r_inv):
    pk_file = get_dir() + "/public_key.pem"
    key = RSA.importKey(open(pk_file).read())
    blinder = PSS_BlindSigScheme(key)
    return blinder.unblind_message(blinded, r_inv)


def new_scheme():
    message = 'hello world'
    [blinded_message, r_inv] = blind(message)
    blinded_signature = new_sign(blinded_message)
    signature = unblind(blinded_signature, r_inv)
    verify_orig(message, signature, "BLIND SCHEME")
    return signature


def test_hash_from_file():
    file = get_dir() + "/test.txt"
    mhash = SHA256.new()
    mhash = generate_file_hash_obj(mhash, file)
    print("COMPUTED MESSAGE DIGEST = " + mhash.hexdigest())
    file_hash = get_dir() + "/hash"
    with open(file_hash) as f:
        print(f.readline())



def sign_orig2():
    sk_file = get_dir() + "/private_key.pem"
    key = RSA.importKey(open(sk_file).read())
    file = get_dir() + "/test.txt"
    mhash = generate_file_hash_obj(SHA256.new(), file)
    signer = PKCS1_PSS.new(key)
    signature = signer.sign(mhash)
    return signature


def write_file(file, sig):
    signature_file = open(file, 'wb')  # good
    signature_file.write(sig)

def test_rsa_pss_sig_on_file():
    sig = sign_orig2()
    import base64
    print(base64.b64encode(sig))
    file = get_dir() + "/mytest.sig"
    write_file(file, sig)


def main():
    original()
    sig2 = new_scheme()
    test_hash_from_file()
    test_rsa_pss_sig_on_file()

    file2 = get_dir() + "/mytest2.sig"
    write_file(file2, sig2)


if __name__ == "__main__":
    main()