import unittest
from cli import backend
from Crypto.PublicKey import RSA


class TestRSAPSS(unittest.TestCase):

    rsa_pss = None
    rsa_backend = None
    pk = None
    sk = None


    """
    def setUp(self):
        self.__class__.rsa_backend = backend.RSABackend()
        self.__class__.rsa_backend.create_scheme(hash_type="SHA256", key_size=2048, public_exponent=65537)
        self.__class__.rsa_pss = backend.RSAPSS(hash_type="SHA256", key_size=2048, public_exponent=65537)

    def test_check_values(self):
        self.assertIsInstance(self.__class__.rsa_pss, backend.RSAPSS)
        self.assertIsInstance(self.__class__.rsa_backend, backend.RSABackend)
        self.assertEqual(self.__class__.rsa_pss._public_exponent, self.__class__.rsa_backend._scheme._public_exponent)

    def test_generate_keys(self):
        self.assertIsInstance(self.__class__.rsa_pss, backend.RSAPSS)
        [pk, sk] = self.__class__.rsa_pss.generate_keys()
        self.assertIsInstance(self.__class__.rsa_pss._key, RSA._RSAobj)
        self.__class__.rsa_pss._key = sk
        self.__class__.pk = pk
        self.__class__.sk = sk

        self.assertIsInstance(self.__class__.sk, RSA._RSAobj)
        self.assertIsInstance(self.__class__.pk, RSA._RSAobj)
        self.assertIsInstance(self.__class__.rsa_pss._key, RSA._RSAobj)

        public_key = self.__class__.rsa_pss._key.publickey()
        self.assertIsInstance(public_key, RSA._RSAobj)

        self.assertEqual(self.__class__.rsa_pss._key.n, public_key.n, "Public modulus unequal")
        self.assertEqual(self.__class__.rsa_pss._key.e, public_key.e, "Public exponent unequal")

    """


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestRSAPSS)
    unittest.TextTestRunner(verbosity=2).run(suite)