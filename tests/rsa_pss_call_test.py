
from PyBlindSchemes.Blindsignature.rsa_pss import PSS_BlindSigScheme
from Crypto.Hash import SHA3_512
import os


def get_dir(my_dir='keys'):
    return os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, my_dir))


def test(key):
    rsapss = PSS_BlindSigScheme(key)
    msg = 'hello world'
    mhash = SHA3_512.new(msg.encode())

    [blind_msg, r_inv] = rsapss.blind_message(mhash)
    blind_sig = rsapss.sign_message(blind_msg)
    sig = rsapss.unblind_message(blind_sig, r_inv)
    try:
        rsapss.verify_message(mhash, sig)
        print("Verified OK")
    except ValueError as e:
        print(e)


def load():
    sk = get_dir() + '/private_key.pem'
    key = PSS_BlindSigScheme.import_key(sk)
    test(key)


def generate():
    [pk, sk] = PSS_BlindSigScheme.generate_keys(2048)
    test(sk)
    # test(pk)


def test_one():
    load()
    generate()


def main():
    test_one()


if __name__ == "__main__":
    main()
