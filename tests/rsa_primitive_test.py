import unittest
import os
from cli.rsabackend import RSA_Backend
from cli.rsapssbackend import RSA_PSS_Backend
from Crypto.PublicKey import RSA
from cli.defaults import *
from PyBlindSchemes.Blindsignature.rsa_pss import PSS_BlindSigScheme
from io import StringIO
import sys
from contextlib import contextmanager


class TestRSAPSS(unittest.TestCase):

    file_list = None
    rsa_pss_backend = None
    message = None
    inverse_factor = None
    hash_function = None
    blind_message = None
    blind_signature = None
    right_password = None

    def setUp(self):
        self.__class__.file_list = {
            'rsa_key': 'rsa_key.bin',
            'rsa_public_pem': 'public_key.pem',
            'rsa_private_pem': 'private_key.pem',
            'blind_message': 'blind_message.bin',
            'blind_signature': 'blind_signature.bin',
            'signature': 'signature.bin',
            'inverse': 'blind_message.bin.inv'
        }
        self.__class__.message = 'secret message'
        self.__class__.hash_function = 'SHA512'
        self.__class__.right_password = 'password'

    def test_a_crypto_rsa_gen(self):

        secret_code = "strongpassword12345"
        key = RSA.generate(2048)
        encrypted_key = key.export_key(passphrase=secret_code, pkcs=8)
        key_file_name = self.__class__.file_list['rsa_key']
        file_out = open(key_file_name, "wb")
        file_out.write(encrypted_key)
        file_out.close()
        file_in = open(key_file_name, "rb")
        encoded_key = file_in.read()
        file_in.close()
        key = RSA.import_key(encoded_key, passphrase=secret_code)
        self.assertIsInstance(key, RSA.RsaKey)
        try:
            RSA.import_key(encoded_key, passphrase='incorrect_password')
        except ValueError:
            pass

    def test_b_rsabackend_gen(self):
        """" Generate keys
        """
        rsa_backend = RSA_Backend()
        password = self.__class__.right_password
        self.assertEqual(password, 'password')
        public_file_name = self.__class__.file_list['rsa_public_pem']
        private_file_name = self.__class__.file_list['rsa_private_pem']
        data = {

            ARGUMENT_KEY_KEY_FORMAT: 'PEM',
            ARGUMENT_KEY_SIG_OPT: {
                SIG_OPT_RSA_KEY_BITS: 2048,
                SIG_OPT_RSA_KEY_E: 65537,
                SIG_OPT_PKCS: 1,
                SIG_OPT_PROTECTION: None
            },
            ARGUMENT_KEY_PUBLIC_KEY: public_file_name,
            ARGUMENT_KEY_PRIVATE_KEY: private_file_name,
            ARGUMENT_NAME_PRIVATE_KEY_PASS: password
        }
        rsa_backend.generate_keys(**data)

        """ Load private key with right and wrong password
        """
        rsa_pss_backend = RSA_PSS_Backend()
        data2 = {
            ARGUMENT_KEY_PRIVIN: private_file_name,
            ARGUMENT_KEY_KEY_FORMAT: 'PEM',
            ARGUMENT_KEY_PRIVATE_KEY_PASS: password
        }
        scheme = rsa_pss_backend.prepare_scheme(ARGUMENT_KEY_PRIVIN, **data2)
        rsa_pss_backend = RSA_PSS_Backend(scheme)
        self.assertIsInstance(rsa_pss_backend, RSA_PSS_Backend)
        self.assertIsInstance(scheme, PSS_BlindSigScheme)

        """ Attempt to load private key with bad password
        """
        data3 = {
            ARGUMENT_KEY_PRIVIN: private_file_name,
            ARGUMENT_KEY_KEY_FORMAT: 'PEM',
            ARGUMENT_KEY_PRIVATE_KEY_PASS: 'incorrect_password'
        }
        try:
            rsa_pss_backend.prepare_scheme(ARGUMENT_KEY_PRIVIN, **data3)
        except ValueError:
            pass

    def test_c_blind_message(self):
        rsa_pss_backend = RSA_PSS_Backend()
        data = {
            ARGUMENT_KEY_HASH_FUNCTION: self.__class__.hash_function,
            ARGUMENT_KEY_FILE: 'message.txt',
            ARGUMENT_KEY_KEY_FORMAT: 'PEM',
            ARGUMENT_KEY_PUBIN: self.__class__.file_list['rsa_public_pem'],
            ARGUMENT_KEY_OUT: self.__class__.file_list['blind_message']
        }
        rsa_pss_backend.blind(None, **data)

    def test_d_sign_message(self):
        password = self.__class__.right_password
        rsa_pss_backend = RSA_PSS_Backend()
        data = {
            ARGUMENT_KEY_PRIVIN: self.__class__.file_list['rsa_private_pem'],
            ARGUMENT_KEY_FILE: self.__class__.file_list['blind_message'],
            ARGUMENT_KEY_OUT: self.__class__.file_list['blind_signature'],
            ARGUMENT_KEY_KEY_FORMAT: 'PEM',
            ARGUMENT_KEY_PRIVATE_KEY_PASS: password
        }
        rsa_pss_backend.sign(None, **data)

    def test_e_unblind_message(self):
        rsa_pss_backend = RSA_PSS_Backend()
        data = {
            ARGUMENT_KEY_PUBIN: self.__class__.file_list['rsa_public_pem'],
            ARGUMENT_KEY_FILE: self.__class__.file_list['blind_signature'],
            ARGUMENT_KEY_OUT: self.__class__.file_list['signature'],
            ARGUMENT_KEY_KEY_FORMAT: 'PEM',
            ARGUMENT_KEY_IN: self.__class__.file_list['inverse'],
        }
        rsa_pss_backend.unblind(None, **data)

    def test_f_verify_message(self):
        rsa_pss_backend = RSA_PSS_Backend()
        data = {
            ARGUMENT_KEY_PUBIN: self.__class__.file_list['rsa_public_pem'],
            ARGUMENT_KEY_IN: self.__class__.file_list['signature'],
            ARGUMENT_KEY_HASH_FUNCTION: self.__class__.hash_function,
            ARGUMENT_KEY_FILE: 'message.txt',
            ARGUMENT_KEY_KEY_FORMAT: 'PEM',
        }
        with self.capture(rsa_pss_backend.verify, None, **data) as output:
            self.assertEqual("Verification OK", output.strip())

    @contextmanager
    def capture(self, command, *args, **kwargs):
        out, sys.stdout = sys.stdout, StringIO()
        try:
            command(*args, **kwargs)
            sys.stdout.seek(0)
            yield sys.stdout.read()
        finally:
            sys.stdout = out

    def delete_files(self):
            if self.__class__.file_list is not None:
                for key, value in self.__class__.file_list.items():
                    if os.path.isfile(value):
                        print('Deleting {0}'.format(value))
                        os.remove(value)

    def test_z_delete(self):
        self.delete_files()
        pass
    """
    def setUp(self):
        self.__class__.rsa_backend = backend.RSABackend()
        self.__class__.rsa_backend.create_scheme(hash_type="SHA256", key_size=2048, public_exponent=65537)
        self.__class__.rsa_pss = backend.RSAPSS(hash_type="SHA256", key_size=2048, public_exponent=65537)

    def test_check_values(self):
        self.assertIsInstance(self.__class__.rsa_pss, backend.RSAPSS)
        self.assertIsInstance(self.__class__.rsa_backend, backend.RSABackend)
        self.assertEqual(self.__class__.rsa_pss._public_exponent, self.__class__.rsa_backend._scheme._public_exponent)

    def test_generate_keys(self):
        self.assertIsInstance(self.__class__.rsa_pss, backend.RSAPSS)
        [pk, sk] = self.__class__.rsa_pss.generate_keys()
        self.assertIsInstance(self.__class__.rsa_pss._key, RSA._RSAobj)
        self.__class__.rsa_pss._key = sk
        self.__class__.pk = pk
        self.__class__.sk = sk

        self.assertIsInstance(self.__class__.sk, RSA._RSAobj)
        self.assertIsInstance(self.__class__.pk, RSA._RSAobj)
        self.assertIsInstance(self.__class__.rsa_pss._key, RSA._RSAobj)

        public_key = self.__class__.rsa_pss._key.publickey()
        self.assertIsInstance(public_key, RSA._RSAobj)

        self.assertEqual(self.__class__.rsa_pss._key.n, public_key.n, "Public modulus unequal")
        self.assertEqual(self.__class__.rsa_pss._key.e, public_key.e, "Public exponent unequal")

    """


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestRSAPSS)
    unittest.TextTestRunner(verbosity=2).run(suite)