# PyBlindSchemes

## Prikazy

### Generovanie klucov

Generovanie RSA klucov (default):
    - key format: PEM, 
    - key length: 2048,
    - public key exponent: 65537,
    - filename for PUBLIC KEY: public_key.pem,
    - filename for PRIVATE KEY: private_key.pem,
```
pyblind rsa gen
```

To iste ako predtym, iba pre format DER:
```
pyblind rsa gen --key-format DER
```

Alebo pre OpenSSH format, kde:
    - default nazov pre sukromny kluc: private_key
    - default nazov pre verejny kluc: public_key.pub

```
pyblind rsa gen --key-format OpenSSH
```
Prepinac `--nopass` vypina pouzitie hesla pri sukromnom kluci (pri operacii `gen` alebo `sign`)


### Zaslepenie spravy
```
pyblind rsa blind -p pss --file <FILENAME> --pubin <PUBLIC_KEY> --digest HASH_FUNCTION --out <BLIND_MESSAGE>
```
napr.
```
pyblind rsa blind -p pss --file message.txt --pubin public_key.pem --digest SHA512 --out blindmsg.bin
```
Po vykonani sa ulozi tiez inverzne cislo do: `blindmsg.bin.inv`


### Podpisanie spravy
```
pyblind rsa sign -p pss --privin <FILENAME> --file <FILENAME> --out <FILENAME>
```
napr.
```
pyblind rsa sign -p pss --privin private_key.pem --file blindmsg.bin --out blindsig.bin
```

### Odslepenie spravy
Pre tuto operaciu:
    - `--file` Nazov suboru na odslepenie
    - `--in` Nazov suboru s inverznym cislom
    - `--pubin` Verejny kluc
    - `--out` Nazov pre odslepeny podpis
```
pyblind rsa unblind -p pss --file <FILENAME> --in <FILENAME> --out <FILENAME> --pubin <FILENAME>
```
napr.
```
pyblind rsa unblind -p pss --file blindsig.bin --in blindmsg.bin.inv --out signature.bin --pubin public_key.pem
```


### OVERENIE SPRAVY
```
pyblind rsa verify -p pss --digest HASH_FUNCTION --in <SIGNATURE> --file <FILE> 
```

napr.
```
pyblind rsa verify -p pss --digest SHA512 --in signature.bin --pubin public_key.pem --file message.txt 
```


## OPENSSL PRIKAZY
### OVERENIE SPRAVY
```
openssl dgst -sha512 -sigopt rsa_padding_mode:pss -sigopt rsa_pss_saltlen:-1 -signature <SIGNATURE> -verify <KEY> <FILE>
```

napr:
```
openssl dgst -sha512 -sigopt rsa_padding_mode:pss -sigopt rsa_pss_saltlen:-1 -signature signature.bin -verify public_key.pem message.txt
```

### overenie verejneho RSA kluca v PEM formate
```
openssl rsa -pubin -text -in key.pem
```
### overenie verejneho RSA kluca v PEM formate
```
openssl rsa -text -in key.pem
```
### overenie verejeneho RSA kluca v DER formate
```
openssl rsa -pubin -text -inform DER -in keyname.der
```
### overenie sukromneho RSA kluca v DER formate
```
openssl rsa -text -inform DER -in keyname.der
```

# OPEN-SSH prikazy
### overenie verejneho kluca
```
ssh-keygen -l -f rsa.pub
```
### overenie sukromneho kluca
```
ssh-keygen -l -f rsa
```

