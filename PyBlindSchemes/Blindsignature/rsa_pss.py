# ===================================================================
#
# Copyright (c) 2018, Martin Dicky <martindicky@gmail.com>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# ===================================================================

# from __builtin__ import super
from Crypto.PublicKey.RSA import RsaKey

from PyBlindSchemes.Blindschemes.rsablindscheme import RSABlindScheme
from PyBlindSchemes.Utils.rsautils import generate_blind_factor
from PyBlindSchemes.Blindsignature.rsa_pss_bs import new, PSS_SigScheme


class PSS_BlindSigScheme(RSABlindScheme):
    """A blind signature wrapper object for ``RSASSA-PSS``.

    """
    def __init__(self, key):
        super(PSS_BlindSigScheme, self).__init__()
        self._key = key
        if isinstance(key, RsaKey):
            self._rsa_pss_bs = new(key)
        else:
            self._rsa_pss_bs = None
        self._blind_factors = None

    def blind_message(self, mHash):
        self._blind_factors = generate_blind_factor(self._rsa_pss_bs.get_key())
        return [self._rsa_pss_bs.blind(mHash, self._blind_factors[0]), self._blind_factors[1]]

    def unblind_message(self, message, r_inv):
        # check blind scheme
        self._check_blind_scheme()
        return self._rsa_pss_bs.unblind(message, r_inv)

    def sign_message(self, message):
        # check blind scheme
        self._check_blind_scheme()
        return self._rsa_pss_bs.sign_raw(message)

    def verify_message(self, mHash, signature):
        self._check_blind_scheme()
        return self._rsa_pss_bs.verify(mHash, signature)

    def _check_blind_scheme(self):
        if self._rsa_pss_bs is None or not isinstance(self._rsa_pss_bs, PSS_SigScheme):
            raise TypeError("RSA_PSS_BS object not set")
