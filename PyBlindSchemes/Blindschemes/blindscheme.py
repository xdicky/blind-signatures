# -*- coding: utf-8 -*-
# ===================================================================
#
# Copyright (c) 2018, Martin Dicky <martindicky@gmail.com>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# ===================================================================

from abc import ABCMeta, abstractmethod


class BlindScheme:

    """ Basic abstract class which defines basic methods for blind signature scheme
    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def generate_keys(self):
        """ Generate private and public keys

            Returns:
                The private and the public key
        """
        raise NotImplementedError('users must define {0} to use this base class'.format('generate_keys'))

    @abstractmethod
    def blind_message(self, hash_message):
        """Blinds the message using his hash value

        Args:
            hash_message (byte string):
                Original message as byte string

        Returns:
            byte string: the blinded message
        """
        raise NotImplementedError('users must define {0} to use this base class'.format('blind_message'))

    @abstractmethod
    def unblind_message(self, blind_signature, inverse_factor):
        """Unblinds the blinded signature with inverse factor

        Args:
            blind_signature (byte string):
                Blinded signature

            inverse_factor (integer):
                Inverse element for unblinding the signature

        Returns:
            byte string: the encoded key
        """
        raise NotImplementedError('users must define {0} to use this base class'.format('unblind_message'))

    @abstractmethod
    def sign_message(self, blind_message):
        """Sign the blinded message

        Args:
            blind_message (byte string):
                Blinded message

        Returns:
            byte string: the encoded key
        """
        raise NotImplementedError('users must define {0} to use this base class'.format('sign_message'))

    @abstractmethod
    def verify_message(self, hash_message, signature):
        """Sign the blinded message

        Args:
            hash_message (byte string):
                Hash object of the original message

            signature (byte string):
                Signature of the original message

        Returns:
            byte string: the encoded key
        """
        raise NotImplementedError('users must define {0} to use this base class'.format('verify_keys'))
