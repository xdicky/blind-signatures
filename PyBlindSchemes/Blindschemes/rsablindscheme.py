# ===================================================================
#
# Copyright (c) 2018, Martin Dicky <martindicky@gmail.com>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# ===================================================================
from PyBlindSchemes.Blindschemes.blindscheme import BlindScheme
from Crypto.PublicKey import RSA


class RSABlindScheme(BlindScheme):

    def __init__(self):
        super(RSABlindScheme, self).__init__()

    @staticmethod
    def generate_keys(key_size, **kwargs):
        """ Generates new public and private RSA keys
        """

        private_key = RSA.generate(key_size, **kwargs)
        public_key = private_key.publickey()
        return [public_key, private_key]

    """ Import/export rsa keys methods
    """
    @staticmethod
    def export_key(key, **kwargs):
        """ Export RSA key.
        """
        return key.export_key(**kwargs)

    @staticmethod
    def _check_key_format(key_format):
        valid_formats = ['PEM', 'DER', 'OpenSSH']
        if key_format not in valid_formats:
            raise ValueError("Unsupported key format.")

    @staticmethod
    def _find_mode_for_key(key_format='PEM'):
        RSABlindScheme._check_key_format(key_format)
        if key_format == "PEM" or key_format == "OpenSSH":
            return "r"
        if key_format == "DER":
            return "rb"

    @staticmethod
    def import_key(filename, key_format='PEM', passphrase=None):
        mode = RSABlindScheme._find_mode_for_key(key_format)
        with open(filename, mode) as f:
            # print(filename, mode, passphrase)
            try:
                return RSA.import_key(f.read(), passphrase=passphrase)
            except ValueError:
                raise ValueError("Can not import RSA key. Maybe padding is incorrect "
                                 "or you have entered wrong password.")


    """ These methods are not implemented here, you should use some padding scheme instead.
    """
    def blind_message(self, hash_message):
        raise NotImplementedError('users must define {0} to use this base class'.format('blind_message'))

    def unblind_message(self, blind_signature, inverse_factor):
        raise NotImplementedError('users must define {0} to use this base class'.format('unblind_message'))

    def sign_message(self, blind_message):
        raise NotImplementedError('users must define {0} to use this base class'.format('sign_message'))

    def verify_message(self, hash_message, signature):
        raise NotImplementedError('users must define {0} to use this base class'.format('generate_keys'))
