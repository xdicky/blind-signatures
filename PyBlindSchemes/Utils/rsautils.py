# ===================================================================
#
# Copyright (c) 2018, Martin Dicky <martindicky@gmail.com>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# ===================================================================

from math import gcd
from Crypto.Random import get_random_bytes
from Crypto.PublicKey.RSA import RsaKey


def _modinv(e, m):
    """ Modular Multiplicative Inverse. Returns x such that: (x*e) mod m == 1
    Used from cryptograph.io

    :Parameters:
        e: Element for inversion
        m: Public modulus
    :return: Modular multiplicative inverse element x
    """
    x1, y1, x2, y2 = 1, 0, 0, 1
    a, b = e, m
    while b > 0:
        q, r = divmod(a, b)
        xn, yn = x1 - q * x2, y1 - q * y2
        a, b, x1, y1, x2, y2 = b, r, x2, y2, xn, yn
    return x1 % m


def _blind_factor(public_key):
    """ Generates blind factor for RSA blind schemes

    :Parameters:
          public_key: an RSA key object
            If a private half is given, both signature and
            verification are possible.
            If a public half is given, only verification is possible.
    :return: blind factor r
    :raise ValueError: if the RSA key is not instance of :class:`Crypto.PublicKey.RSA.RsaKey`
    """
    if not isinstance(public_key, RsaKey):
        raise ValueError("Public key not instance of Crypto.PublicKey.RSA.RsaKey")

    r_size = public_key.size_in_bytes()
    n = public_key.n
    b = 0
    candidate = None
    while b != 1:
        candidate = int(get_random_bytes(r_size).hex(), 16)
        b = gcd(candidate, n)
    return candidate


def generate_blind_factor(public_key):
    """ Returns blind factors r, r^-1, where next equation holds:
    r * r^-1 = 1 mod n, where n is public modulus RSA

    :Parameters:
        public_key: RSA public key
        public_n: RSA public modulus
        key_size: Size of the key in bits
    :return: Blind factors r, r^-1
    """
    r = _blind_factor(public_key)
    r_inv = _modinv(r, public_key.n)
    assert r * r_inv % public_key.n == 1, "Random number does not hold equation r*r-1 = 1 mod n"
    return [r, r_inv]
