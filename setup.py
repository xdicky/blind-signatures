from setuptools import setup, find_packages  # Always prefer setuptools over distutils

setup(
    name='PyBlindSchemes',
    # Versions should comply with PEP440.  For a discussion on single-sourcing
    # the version across setup.py and the project code, see
    # http://packaging.python.org/en/latest/tutorial.html#version
    version='1.0.1',
    description='Lightweight extension of PyCrypto for blind signatures',

    # The project's main homepage.
    url='https://gitlab.com/xdicky/blind-signatures/',
    # Author details
    author='Martin Dicky',


    # See https://PyPI.python.org/PyPI?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: MIT License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3.6.1',
    ],
    packages=find_packages(exclude=['keys', 'tests']),
    install_requires=['pycryptodome'],

    entry_points={
        'console_scripts': [
            'pyblind = cli.main:main'
        ]
    }
)
