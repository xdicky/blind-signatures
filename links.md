### Real world examples
##### David Chaum's project DigiCash (1994 - 1998) 
[David Chaum's project DigiCash](https://web.archive.org/web/19980201190532/http://www.digicash.com:80/index_e.html)

[Beginning of DigiCash](https://www.wired.com/1994/12/emoney/)

[End of DigiCash](http://themonetaryfuture.blogspot.sk/2009/04/how-digicash-blew-everything.html)

[Tedium - Cashing In Early - Overview of Chaum's DigiCash](https://tedium.co/2017/11/27/digicash-ecash-bitcoin-history/)

##### Microsoft U-Prove protocol (2011 - Now)
[Microsoft UProve](https://www.microsoft.com/en-us/research/project/u-prove/)

##### Australian company digi.cash (2016 - Now)
[Digi.Cash](https://www.digi.cash/)



