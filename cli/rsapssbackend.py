from cli.rsabackend import RSA_Backend
from PyBlindSchemes.Blindsignature.rsa_pss import PSS_BlindSigScheme
from cli.defaults import *
from cli.utils import (get_hash_object, generate_file_hash_obj, write_file, read_file)

from Crypto.Util.number import (long_to_bytes, bytes_to_long)
import base64


class RSA_PSS_Backend(RSA_Backend):

    def __init__(self, scheme=None):
        super(RSA_PSS_Backend, self).__init__(scheme)

    def prepare_scheme(self, key_arg, **kwargs):
        key_file = kwargs.pop(key_arg)
        key_format = kwargs.pop(ARGUMENT_KEY_KEY_FORMAT)
        passphrase = kwargs.pop(ARGUMENT_KEY_PRIVATE_KEY_PASS, None)

        key = PSS_BlindSigScheme.import_key(key_file, key_format=key_format,  passphrase=passphrase)
        return PSS_BlindSigScheme(key)

    def prepare_hash_obj(self, **kwargs):
        hash_type = kwargs.pop(ARGUMENT_KEY_HASH_FUNCTION)
        filename = kwargs.pop(ARGUMENT_KEY_FILE)
        return generate_file_hash_obj(get_hash_object(hash_type), filename)

    def blind(self, *args, **kwargs):
        # Step 1 - Prepare hash object for blinding
        hash_obj = self.prepare_hash_obj(**kwargs)

        # Step 2 - Prepare PSS_BlindSigSCheme
        rsa_scheme = self.prepare_scheme(ARGUMENT_KEY_PUBIN, **kwargs)

        # Step 3 - Blind message
        [blind_message, r_inv] = rsa_scheme.blind_message(hash_obj)

        # Step 4 - Encode r_inv to base64
        r_inv_bytes = long_to_bytes(r_inv)
        r_inv_base64 = base64.encodebytes(r_inv_bytes)

        # Step 5 - Export to files
        filename_out = kwargs.pop(ARGUMENT_KEY_OUT)
        write_file(filename_out, blind_message)
        write_file(filename_out+".inv", r_inv_base64)

        print("Blinded message saved in {0}".format(filename_out))
        print("Inverse factor saved in {0}".format(filename_out+".inv"))

    def sign(self, *args, **kwargs):
        rsa_scheme = self.prepare_scheme(ARGUMENT_KEY_PRIVIN, **kwargs)
        filename = kwargs.get(ARGUMENT_KEY_FILE)
        em = read_file(filename, 'rb')
        blind_signature = rsa_scheme.sign_message(em)

        filename_out = kwargs.get(ARGUMENT_KEY_OUT)
        write_file(filename_out, blind_signature)
        print("Blind signature saved in {0}".format(filename_out))

    def unblind(self, *args, **kwargs):
        rsa_scheme = self.prepare_scheme(ARGUMENT_KEY_PUBIN, **kwargs)

        blind_sig_file = kwargs.get(ARGUMENT_KEY_FILE)
        blind_sig = read_file(blind_sig_file, 'rb')

        r_inv_base64_file = kwargs.get(ARGUMENT_KEY_IN)
        r_inv_base64 = read_file(r_inv_base64_file, 'rb')
        r_inv_bytes = base64.decodebytes(r_inv_base64)
        r_inv = bytes_to_long(r_inv_bytes)

        signature = rsa_scheme.unblind_message(blind_sig, r_inv)

        # Export signature
        filename_out = kwargs.get(ARGUMENT_KEY_OUT)
        write_file(filename_out, signature)
        print("Signature saved in {0}".format(filename_out))

    def verify(self, *args, **kwargs):
        rsa_scheme = self.prepare_scheme(ARGUMENT_KEY_PUBIN, **kwargs)
        hash_obj = self.prepare_hash_obj(**kwargs)
        sig_file = kwargs.get(ARGUMENT_KEY_IN)
        signature = read_file(sig_file, 'rb')
        try:
            rsa_scheme.verify_message(hash_obj, signature)
            print("Verification OK")
        except ValueError:
            print("Verification Failure")