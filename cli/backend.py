# -*- coding: utf-8 -*-
""" Backend module for blind signature schemes

"""


class Backend(object):
    """Holds all necessary objects for running blind scheme protocol

    """
    def __init__(self, scheme=None):
        self._scheme = scheme

    def check_scheme(self):
        if self._scheme is None:
            raise ValueError("Backend has no scheme")

    def generate_keys(self, *args, **kwargs):
        raise NotImplementedError('Backend must implement method: generate_keys')

    def sign(self, *args, **kwargs):
        raise NotImplementedError('Backend must implement method: sign')

    def verify(self, *args, **kwargs):
        raise NotImplementedError('Backend must implement method: verify')

    def blind(self, *args, **kwargs):
        raise NotImplementedError('Backend must implement method: blind')

    def unblind(self, *args, **kwargs):
        raise NotImplementedError('Backend must implement method: sign')
