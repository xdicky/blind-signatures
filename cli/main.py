"""
Martin Dicky,
Blind singnatures utility
ver 0.0.4
"""

from cli.parse import parse
from cli.defaults import *
from cli.backend import Backend
from cli.rsabackend import RSA_Backend
from cli.rsapssbackend import RSA_PSS_Backend
from cli.utils import e_print
import argparse


class Program(object):

    def __init__(self, backend):
        self.backend = backend

    @staticmethod
    def create_backend(**kwargs):
        scheme = kwargs.get(ARGUMENT_KEY_SCHEME)
        padding = kwargs.get(ARGUMENT_KEY_PADDING)

        if scheme == SCHEME_RSA:
            if padding is None:
                return RSA_Backend()
            if padding == PADDING_PSS:
                return RSA_PSS_Backend()

        raise ValueError("No backend found")

    def _check_backend(self):
        if self.backend is None:
            raise ValueError("No backend was set")
        if not isinstance(self.backend, Backend):
            raise ValueError("Chosen backend is not instance of class Backend")

    def run(self, *args, **kwargs):
        self._check_backend()
        action = kwargs.get(ARGUMENT_KEY_ACTION)
        if action == ACTION_GEN:
            self.backend.generate_keys(*args, **kwargs)
        if action == ACTION_BLIND:
            self.backend.blind(*args, **kwargs)
        if action == ACTION_SIGN:
            self.backend.sign(*args, **kwargs)
        if action == ACTION_UNBLIND:
            self.backend.unblind(*args, **kwargs)
        if action == ACTION_VERIFY:
            self.backend.verify(*args, **kwargs)


def main():
    try:
        settings = parse()
        # print (settings)

        backend = Program.create_backend(**settings)
        program = Program(backend)
        program.run(**settings)
        exit(0)
    except ValueError as e:
        e_print(e)
    except TypeError as e:
        e_print(e)
    except argparse.ArgumentTypeError as e:
        # parser.print_help()
        e_print(e)
        e_print("Try -h, or --help for printing help message")
    except KeyboardInterrupt as e:
        e_print()
        e_print(e)


if __name__ == '__main__':
    main()

