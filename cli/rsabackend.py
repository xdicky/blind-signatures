from cli.backend import Backend
from cli.defaults import *
from PyBlindSchemes.Blindschemes.rsablindscheme import RSABlindScheme
from cli.utils import (write_file, e_print)


class RSA_Backend(Backend):

    def __init__(self, scheme=None):
        super(RSA_Backend, self).__init__(scheme)

    def generate_keys(self, *args, **kwargs):

        key_format = kwargs.pop(ARGUMENT_KEY_KEY_FORMAT)
        sig_opt = kwargs.pop(ARGUMENT_KEY_SIG_OPT)
        key_size = sig_opt.pop(SIG_OPT_RSA_KEY_BITS)
        public_exponent = sig_opt.pop(SIG_OPT_RSA_KEY_E)
        pkcs = sig_opt.pop(SIG_OPT_PKCS)
        protection = sig_opt.pop(SIG_OPT_PROTECTION)

        public_filename = kwargs.pop(ARGUMENT_KEY_PUBLIC_KEY)
        private_filename = kwargs.pop(ARGUMENT_KEY_PRIVATE_KEY)

        passphrase = kwargs.pop(ARGUMENT_KEY_PRIVATE_KEY_PASS, None)

        try:
            [public, private] = RSABlindScheme.generate_keys(int(key_size), e=int(public_exponent))
        except ValueError:
            raise ValueError(
                "Error during generating RSA key. "
                "You are passing incompatible settings. "
                "Make sure, that *key_size* is the multiple of 256 "
                "and is at least 1024 or public exponent "
                "should be an odd number"
            )

        # try:
        self._check_pkcs(pkcs)
        my_args = {
            'format': key_format,
            'passphrase': passphrase,
            'pkcs': pkcs,
            'protection': protection,
            'randfunc': None
        }
        private_exported = RSABlindScheme.export_key(private, **my_args)

        my_args = {'format': key_format, 'pkcs': pkcs, 'protection': protection}
        public_exported = RSABlindScheme.export_key(public, **my_args)

        """
        except ValueError:
            raise ValueError(
                "Error during exporting RSA key. Key format is unknown or you are trying to encrypt "
                "a private key with DER format and PKCS#1"
            )
        """
        try:
            write_file(public_filename, public_exported)
            print("RSA public key successfully saved in {0}".format(public_filename))
        except FileNotFoundError:
            raise FileNotFoundError("Can not write public key. No such file or directory: {0}".format(public_filename))

        try:
            write_file(private_filename, private_exported)
            print("RSA private key successfully saved in {0}".format(private_filename))
        except FileNotFoundError:
            raise FileNotFoundError("Can not write private key. No such file or directory: {0}"
                                    .format(private_filename))

    def _check_pkcs(self, pkcs):
        if int(pkcs) != 1 and int(pkcs) != 8:
            raise ValueError("Invalid pkcs value. Valid values are: 1, 8")

    def sign(self, *args, **kwargs):
        raise NotImplementedError('Backend must implement method: sign')

    def verify(self, *args, **kwargs):
        raise NotImplementedError('Backend must implement method: verify')

    def blind(self, *args, **kwargs):
        raise NotImplementedError('Backend must implement method: blind')

    def unblind(self, *args, **kwargs):
        raise NotImplementedError('Backend must implement method: sign')

