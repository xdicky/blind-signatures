# ===================================================================
#
# Copyright (c) 2018, Martin Dicky <martindicky@gmail.com>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# ===================================================================

# from __future__ import print_function
import sys
from Crypto.Hash import (SHA3_512, SHA3_384, SHA3_256, SHA3_224, SHA512, SHA384, SHA256, SHA224)
from cli.defaults import (DEFAULT_SHA2_FUNCTIONS, DEFAULT_SHA3_FUNCTIONS)


def read_file(filename, mode=None):
    if mode is None:
        mode = "r"
    with open(filename, mode) as f:
        return f.read()


def e_print(*args, **kwargs):
    """ Prints to stderr
    """
    print(*args, file=sys.stderr, **kwargs)


def generate_file_hash_obj(mhash, filename, block_size=2 ** 16):
    with open(filename, "rb") as f:
        m = mhash
        while True:
            buf = f.read(block_size)
            if not buf:
                break
            m.update(buf)
        return m


def valid_hash_functions():
    return DEFAULT_SHA2_FUNCTIONS + DEFAULT_SHA3_FUNCTIONS


def write_file(filename, data, mode="wb"):
    with open(filename, mode) as f:
        f.write(data)

def get_hash_object(hash_type):
    if not isinstance(hash_type, str):
        raise TypeError("Argument is not a string")
    functions = valid_hash_functions()
    if hash_type.upper() not in functions:
        raise ValueError("Unsupported hash function")
    if hash_type == "SHA224":
        return SHA224.new()
    elif hash_type == "SHA256":
        return SHA256.new()
    elif hash_type == "SHA384":
        return SHA384.new()
    elif hash_type == "SHA512":
        return SHA512.new()
    elif hash_type == "SHA3_224":
        return SHA3_224.new()
    elif hash_type == "SHA3_256":
        return SHA3_256.new()
    elif hash_type == "SHA3_384":
        return SHA3_384.new()
    elif hash_type == "SHA3_512":
        return SHA3_512.new()
    else:
        raise ValueError("Unsupported hash function")