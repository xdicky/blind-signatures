import argparse
# import os
from cli.utils import (valid_hash_functions, e_print)
from cli.defaults import *
import os.path
import getpass

class StoreDictKeyPair(argparse.Action):
    """ User can specfiy more KEY=VALUE pairs
    """

    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        self._nargs = nargs
        super(StoreDictKeyPair, self).__init__(option_strings, dest, nargs=nargs, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        my_dict = {}
        for kv in values:
            k, v = kv.split("=")
            my_dict[k] = v
        setattr(namespace, self.dest, my_dict)


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise TypeError('Boolean value expected.')


def _create_args():

    parser = argparse.ArgumentParser(description='Blind signatures utility',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    long_desc = "Set optional settings for operation.\nValid options are:\n" \
                "{0}    Set RSA key size in bits. Default ({1}) " \
                "{2}    Set RSA public exponent. Default ({3}) " \
                "{4}    Set pkcs standard for private key. Default ({5}). Valid values (1, 8) " \
                "{6}    Set protection algorithm for private key. It will used only,\n " \
                "       if you choose pkcs = 8. Default ({7})"\
        .format(
                SIG_OPT_RSA_KEY_BITS, DEFAULT_RSA_KEY_BITS, SIG_OPT_RSA_KEY_E, DEFAULT_RSA_PUBLIC_EXPONENT,
                SIG_OPT_PKCS, 1, SIG_OPT_PROTECTION, DEFAULT_KEY_PROTECTION
        )

    parser.add_argument("--sig-opt", action='append', type=lambda kv: kv.split("=", 1), dest='sig_opt',
                        help=long_desc)

    parser.add_argument(ARGUMENT_NAME_SCHEME, help='Set blind signature scheme', choices=DEFAULT_VALID_SCHEMES)
    parser.add_argument(ARGUMENT_NAME_ACTION, help='Choose action', choices=DEFAULT_VALID_ACTIONS)
    parser.add_argument('-p', ARGUMENT_NAME_PADDING, help='Set padding scheme', choices=DEFAULT_VALID_PADDING)
    parser.add_argument(ARGUMENT_NAME_HASH_FUNCTION, help='Set hash function', choices=valid_hash_functions())

    # parser.add_argument(ARGUMENT_NAME_RSA_KEY_BITS, help='Set bit length for new rsa key')
    # parser.add_argument(ARGUMENT_NAME_RSA_KEY_PUBLIC_E, help='Set public exponent for new rsa key')
    # parser.add_argument('--key-directory', help='Set directory for saving keys and certs')

    parser.add_argument(ARGUMENT_NAME_PUBLIC_KEY, help='Set public key filename')
    parser.add_argument(ARGUMENT_NAME_PRIVATE_KEY, help='Set private key filename')

    parser.add_argument(ARGUMENT_NAME_PRIVATE_KEY_PASS, help='Set passphrase for private key')
    parser.add_argument(ARGUMENT_NAME_NO_KEY_PASS, help='Turn off passphrase private key', action='store_true')

    # parser.add_argument('--sig-encoding', help='Encode')
    parser.add_argument(ARGUMENT_NAME_PUBIN, help='Set public key for operation')
    parser.add_argument(ARGUMENT_NAME_PRIVIN, help='Set private key for operation')

    parser.add_argument('-i', ARGUMENT_NAME_IN, help='Set input file for selected action')
    parser.add_argument('-o', ARGUMENT_NAME_OUT, help='Set output file for selected action')
    parser.add_argument(ARGUMENT_NAME_KEY_FORMAT, help='Set key format for import/export',
                        choices=DEFAULT_VALID_KEY_FORMATS)
    parser.add_argument(ARGUMENT_NAME_FILE, help='Set file for action')

    return parser


def _check_args_sig_opt(parse_args):
    """ Checks for valid sig-opt KEYS
    """
    sig_opt = parse_args.get(ARGUMENT_KEY_SIG_OPT, None)
    if sig_opt is not None:
        public_e = sig_opt.pop(SIG_OPT_RSA_KEY_E, None)
        bits = sig_opt.pop(SIG_OPT_RSA_KEY_BITS, None)
        pkcs = sig_opt.pop(SIG_OPT_PKCS, None)
        protection = sig_opt.pop(SIG_OPT_PROTECTION, None)

        if len(sig_opt) > 0:
            raise TypeError('Unexpected sig-opt provided: %s' % list(sig_opt.keys()))

        if bits is not None:
            bits = int(bits)
            if bits % 256 != 0 or bits < 1024:
                raise ValueError('Invalid value for key size in RSA scheme')
    else:
        public_e = None
        bits = None
        pkcs = DEFAULT_PRIVATE_KEY_PKCS
        protection = None

    if public_e is None:
        public_e = DEFAULT_RSA_PUBLIC_EXPONENT
        e_print("INFO: {0} {1} not set. Using default value: {2}"
                .format(ARGUMENT_NAME_SIG_OPT, SIG_OPT_RSA_KEY_E, DEFAULT_RSA_PUBLIC_EXPONENT))
    if bits is None:
        bits = DEFAULT_RSA_KEY_BITS
        e_print("INFO: {0} {1} not set. Using default value: {2}"
                .format(ARGUMENT_NAME_SIG_OPT, SIG_OPT_RSA_KEY_BITS, DEFAULT_RSA_KEY_BITS))

    if pkcs is None:
        pkcs = DEFAULT_PRIVATE_KEY_PKCS

    parse_args[ARGUMENT_KEY_SIG_OPT] = {
        SIG_OPT_RSA_KEY_E: public_e,
        SIG_OPT_RSA_KEY_BITS: bits,
        SIG_OPT_PKCS: pkcs,
        SIG_OPT_PROTECTION: protection
    }
    return parse_args


def _validate_key_format(parse_args):
    key_format = parse_args.get(ARGUMENT_KEY_KEY_FORMAT)
    if key_format is None:
        e_print("INFO: {0} not set. Using default value: {1}".format(ARGUMENT_NAME_KEY_FORMAT, DEFAULT_KEY_FORMAT))
        parse_args[ARGUMENT_KEY_KEY_FORMAT] = DEFAULT_KEY_FORMAT
    else:
        if key_format not in DEFAULT_VALID_KEY_FORMATS:
            raise ValueError("Invalid key format. Please check your --key-format option.")
    return parse_args


def _check_existing_filename(parse_args, argument_key, argument_name):
    filename = parse_args.get(argument_key)
    if filename is None:
        raise ValueError("Missing {0} for this action".format(argument_name))
    if not os.path.isfile(filename):
        raise ValueError("Invalid file path. File does not exists. Please check your {0} option.".format(argument_name))


def _check_hash_type(parse_args):
    hash_type = parse_args.get(ARGUMENT_KEY_HASH_FUNCTION)
    if hash_type not in DEFAULT_VALID_HASH_FUNCTIONS:
        raise ValueError("Invalid hash function. Unknown type. Please check your {0} option."
                         .format(ARGUMENT_NAME_HASH_FUNCTION))


def _validate_default_value(parse_args, arg_key, arg_name, default_value):
    actual = parse_args.get(arg_key)
    if actual is None:
        parse_args[arg_key] = default_value
        e_print("INFO: {0} not set. Using default value: {1}".format(arg_name, default_value))
    return parse_args


def _check_mandatory_arg(parse_args, arg_key, arg_name):
    value = parse_args.get(arg_key)
    if value is None:
        raise ValueError("Missing option {0}. Please check yor {1} option.".format(arg_name, arg_name))


def _validate_file_name_for_key(parse_args, **kwargs):
    arg_key = kwargs.pop('arg_key')
    arg_name = kwargs.pop('arg_name')
    default_value = kwargs.pop('default_value')
    filename = parse_args.get(arg_key)
    key_format = parse_args.get(ARGUMENT_KEY_KEY_FORMAT)
    if filename is None:
        if key_format == 'OpenSSH':
            if arg_name == ARGUMENT_NAME_PRIVATE_KEY:
                default_filename = default_value
            else:
                default_filename = default_value + ".pub"
        else:
            default_filename = default_value + "." + key_format.lower()
        parse_args[arg_key] = default_filename
        e_print("INFO: {0} not set. Using default value: {1}".format(arg_name, default_filename))
    return parse_args


def _validate_passphrase(parse_args):
    """ User does not turn off the flag -> he wants to use passphrase for protection
        """
    nopass = parse_args.pop(ARGUMENT_KEY_NO_KEY_PASS)
    passphrase = parse_args.get(ARGUMENT_KEY_PRIVATE_KEY_PASS, None)
    if not nopass:
        if passphrase is None:
            try:
                parse_args[ARGUMENT_KEY_PRIVATE_KEY_PASS] = getpass.getpass("Passphrase for private key:")
            except KeyboardInterrupt:
                raise KeyboardInterrupt("User interrupts process during passphrase input. Exiting...")
    return parse_args


def _check_rsa_gen(parse_args):

    # Step 1 - Validate possible signature options
    parse_args = _check_args_sig_opt(parse_args)

    # Step 2 - Validate key format
    parse_args = _validate_key_format(parse_args)

    # Step 3 - Validate filename for private key
    private_dict = {
        'arg_key': ARGUMENT_KEY_PRIVATE_KEY,
        'arg_name': ARGUMENT_NAME_PRIVATE_KEY,
        'default_value': DEFAULT_PRIVATE_KEY_FILENAME
    }
    parse_args = _validate_file_name_for_key(parse_args, **private_dict)

    # Step 4 - Validate filename for public key
    private_dict = {
        'arg_key': ARGUMENT_KEY_PUBLIC_KEY,
        'arg_name': ARGUMENT_NAME_PUBLIC_KEY,
        'default_value': DEFAULT_PUBLIC_KEY_FILENAME
    }
    parse_args = _validate_file_name_for_key(parse_args, **private_dict)

    # Step 5 - check passphrase for private key
    parse_args = _validate_passphrase(parse_args)

    return parse_args


def _check_rsa_blind(parse_args):
    # Step 1 - Validate key format
    parse_args = _validate_key_format(parse_args)
    # Step 2 - check filenames for public key and file
    _check_existing_filename(parse_args, ARGUMENT_KEY_FILE, ARGUMENT_NAME_FILE)
    _check_existing_filename(parse_args, ARGUMENT_KEY_PUBIN, ARGUMENT_NAME_PUBIN)
    # Step 3 - check hash function
    _check_hash_type(parse_args)
    # Step 4 - check filename for output
    _check_mandatory_arg(parse_args, ARGUMENT_KEY_OUT, ARGUMENT_NAME_OUT)

    return parse_args


def _check_rsa_sign(parse_args):

    # Step 1 - check filenames for file and private key
    _check_existing_filename(parse_args, ARGUMENT_KEY_FILE, ARGUMENT_NAME_FILE)
    _check_existing_filename(parse_args, ARGUMENT_KEY_PRIVIN, ARGUMENT_NAME_PRIVIN)

    # Step 2 - check filename for output
    _check_mandatory_arg(parse_args, ARGUMENT_KEY_OUT, ARGUMENT_NAME_OUT)

    # Step 3- Validate key format
    parse_args = _validate_key_format(parse_args)

    # Step 4 - check passphrase for private key
    parse_args = _validate_passphrase(parse_args)

    return parse_args


def _check_rsa_unblind(parse_args):
    # Step 1 - Validate key format
    parse_args = _validate_key_format(parse_args)

    # Step 2 - check filenames for source file, output file and input file
    _check_existing_filename(parse_args, ARGUMENT_KEY_FILE, ARGUMENT_NAME_FILE)
    _check_existing_filename(parse_args, ARGUMENT_KEY_PUBIN, ARGUMENT_NAME_PUBIN)
    _check_existing_filename(parse_args, ARGUMENT_KEY_IN, ARGUMENT_NAME_IN)

    # Step 3 - check filename for output
    _check_mandatory_arg(parse_args, ARGUMENT_KEY_OUT, ARGUMENT_NAME_OUT)

    return parse_args


def _check_rsa_verify(parse_args):
    # Step 1 - Validate key format
    parse_args = _validate_key_format(parse_args)

    # Step 2 - check filenames for public key, file and input file
    _check_existing_filename(parse_args, ARGUMENT_KEY_FILE, ARGUMENT_NAME_FILE)
    _check_existing_filename(parse_args, ARGUMENT_KEY_PUBIN, ARGUMENT_NAME_PUBIN)
    _check_existing_filename(parse_args, ARGUMENT_KEY_IN, ARGUMENT_NAME_IN)
    # Step 3 - check hash function
    _check_hash_type(parse_args)

    return parse_args


def _validate_rsa_scheme(parse_args):
    # print(parse_args)
    padding = parse_args.get(ARGUMENT_KEY_PADDING)
    action = parse_args.get(ARGUMENT_KEY_ACTION)
    if action != ACTION_GEN:
        if padding is None:
            raise ValueError("You have to pass padding for this blind scheme: {0}".format(parse_args.get(ARGUMENT_KEY_SCHEME)))
    if action == ACTION_GEN:
        return _check_rsa_gen(parse_args)

    if action == ACTION_BLIND:
        return _check_rsa_blind(parse_args)

    if action == ACTION_SIGN:
        return _check_rsa_sign(parse_args)

    if action == ACTION_UNBLIND:
        return _check_rsa_unblind(parse_args)

    if action == ACTION_VERIFY:
        return _check_rsa_verify(parse_args)


def _validate(parse_args, parser):
    scheme = parse_args.get(ARGUMENT_KEY_SCHEME)
    if scheme == SCHEME_RSA:
        return _validate_rsa_scheme(parse_args)


def _parse():
    parser = _create_args()
    parse_args = parser.parse_args()
    if parse_args.sig_opt is not None:
        if any(len(kv) < 2 for kv in parse_args.sig_opt):
            raise ValueError('Values must be given in the form "KEY=VALUE"')
        parse_args.sig_opt = dict(parse_args.sig_opt)
    return [parser, vars(parse_args)]


def parse():
    [parser, parse_args] = _parse()
    new_settings = _validate(parse_args, parser)
    return new_settings
